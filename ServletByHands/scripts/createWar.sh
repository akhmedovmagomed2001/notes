mkdir -p build/warFile/WEB-INF/classes
mkdir -p build/war

WAR_FILE=build/warFile

cp build/classes/MainServlet.class $WAR_FILE/WEB-INF/classes
cp resources/web.xml $WAR_FILE/WEB-INF

cd $WAR_FILE
jar -cvf ../war/app.war * 

cd ../../ 
rm -rf $WAR_FILE

